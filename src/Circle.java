public class Circle extends Figure{
    double r;
    public Circle(String name, double r){
        super(name);
        this.r=r;
        this.Perimetr = 2*3.14*r;
        this.Plochadj = 3.14*r*r;
    }
    @Override
    public double getPerimetr(){ return Perimetr;}
    public void printer(){
        System.out.printf("\nфигура: %s\nРадиус: %f\nПериметр: %f\nПлощадь: %f",name,r, this.Perimetr, this.Plochadj);
    }
}
