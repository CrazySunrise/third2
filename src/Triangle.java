public class Triangle extends Figure {

    double p;
    public Triangle(String name, int a, int b, int c){
        super(name);
        this.a=a;
        this.b=b;
        this.c = c;
        this.Perimetr = a+b+c;
        p = this.Perimetr/2;
        this.Plochadj = Math.sqrt(p*(p-a)*(p-b)*(p-c));
    }
    @Override
    public double getPerimetr(){ return Perimetr;}
    public void printer(){
        System.out.printf("\nфигура: %s\nОдна сторона: %d\nВторая сторона: %d\nТретья сторона: %d\nПериметр: %f\nПлощадь: %f",name,a,b,c, this.Perimetr, this.Plochadj);
    }
}
