import java.util.Scanner;

public class main {

    public static String Vvod(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        return sc.next();
    }

    public static void main(String[] args) {
        int a = -1;
        while (a != 00) {
            System.out.println("\n1-Круг\n2-Прямоугольник\n3-Треугольник");
            Scanner scanner = new Scanner(System.in);
            a = scanner.nextInt();
            switch (a) {
                case 1:
                    System.out.println("Введите радиус круга");
                    Circle circle = new Circle("Круг", Double.valueOf(Vvod("Радиус")));
                    circle.printer();
                    break;
                case 2:
                    System.out.println("Введите длину и ширину");
                    Rectangle rectangle = new Rectangle("Прямоугольник", Integer.valueOf(Vvod("Ширина")), Integer.valueOf(Vvod("Длина")));
                    rectangle.printer();
                    break;
                case 3:
                    System.out.println("Введите стороны треугольника");
                    Triangle triangle = new Triangle("Треугольник", Integer.valueOf(Vvod("Сторона треугольника")), Integer.valueOf(Vvod("Сторона треугольника")), Integer.valueOf(Vvod("Сторона треугольника")));
                    triangle.printer();
                    break;
            }
        }
    }
}
